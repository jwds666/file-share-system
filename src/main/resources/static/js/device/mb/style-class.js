var styleClass = {

    init:function (){
        fileTable.headers = [
            {name:'文件名', width:'50%'},
            {name:'修改日期', width:'30%'},
            {name:'大小', width:'20%'}
        ]
    },

    tableTdAppend:function (tr,itemData){

        tr.append('<td class="file-name"><div class="mb-fileinfo-div">\n' +
            '    <i class="fa fa-'+itemData.icon+' file-icon" ></i>\n' +
            '    <span class="mb-fileinfo" ><span>'+itemData.name+'</span><br/><span class="file-attribute">'+itemData.fileType+'</span></span>\n' +
            '</div></td>')
        tr.append('<td class="file-attribute">'+itemData.updateTime+'</td>')
        tr.append('<td class="file-attribute">'+itemData.fileType+'</td>')
        var size = ""
        if (itemData.isFile){
            var z = itemData.size%1024 != 0 ? 1:0;
            size = parseInt(itemData.size/1024)+z+"kb"
        }
        tr.append('<td class="file-attribute">'+size+'</td>')
    }


}