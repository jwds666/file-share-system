package com.gitee.jwds666.fs.env;

import com.gitee.jwds666.fs.account.AccountManager;
import com.gitee.jwds666.fs.account.SimpleAccount;
import com.gitee.jwds666.fs.config.FileShareConfig;
import com.gitee.jwds666.fs.utils.FileUtils;
import com.gitee.jwds666.fs.utils.IniUtils;
import com.gitee.jwds666.fs.utils.ReflectUtils;
import com.gitee.jwds666.fs.account.permission.Permission;
import com.gitee.jwds666.fs.account.permission.PermissionResolver;

import java.io.*;
import java.util.*;

public class ConfEnvironment extends AbstractEnvironment {

    public ConfEnvironment(AccountManager accountManager, PermissionResolver permissionResolver, FileShareConfig config) {
        super(accountManager, permissionResolver, config);
    }


    private String iniFileMD5;

    private String confFilename = "share.conf";

    protected InputStream getIniStream() throws IOException {
        String iniPath = confFilename;
        InputStream is = null;
        File conf = new File(iniPath);
        if (!conf.exists()){
            conf.createNewFile();
        }
        try {
            is = new FileInputStream(iniPath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return FileUtils.genericBufferedStream(is);
    }

    /**
     * 运行环境
     */
    @Override
    protected void onRun()  {
        try (InputStream is = getIniStream()){
            onRun(is);
        }catch (Exception e){
            System.out.println("运行失败："+e.getMessage());
        }
    }

    protected void onRun(InputStream is) throws IOException {
        iniFileMD5 = FileUtils.getFileMD5String(is);
        Map<String,List<String>> iniData = IniUtils.ini(is);
        List<String> configStrings = iniData.get("config");
        if (configStrings != null){
            for (String configString : configStrings) {
                ReflectUtils.setField(super.config,configString);
            }
        }
        iniData.remove("config");
        for (Map.Entry<String, List<String>> accountData : iniData.entrySet()) {
            SimpleAccount account = new SimpleAccount();
            account.setToken(accountData.getKey());
            List<String> args = accountData.getValue();
            if (args.size() != 0){
                int i = 0;
                if (ReflectUtils.setField(account,args.get(i))){
                    i++;
                }
                List<Permission> permissions = new ArrayList<>();
                for (; i < args.size(); i++) {
                    permissions.add(permissionResolver.resolve(args.get(i)));
                }
                account.setPermissions(permissions);
            }
            accountManager.add(account);
        }
        ensureBasePath();
        ensureAnonAccount();

    }


    /**
     * 保证有一个暴露的地址
     */
    protected void ensureBasePath(){
        String basePath = config.getBasePath();
        if (basePath == null || basePath.equals("")){
            basePath = "share";
        }
        basePath = basePath.replace("\\\\",FileConstants.SEPARATOR);
        basePath = basePath.replace("//",FileConstants.SEPARATOR);
        basePath = basePath.replace("\\",FileConstants.SEPARATOR);
        if (!basePath.substring(basePath.length()-1).equals(FileConstants.SEPARATOR)){
            basePath+=FileConstants.SEPARATOR;
        }
        File baseDir = new File(basePath);
        if (!baseDir.exists() || baseDir.isFile()){
            baseDir.mkdirs();
        }
        config.setBasePath(basePath);
    }

    /**
     * 保证有一个匿名用户
     */
    protected void ensureAnonAccount(){
        if (accountManager.get("") == null){
            SimpleAccount account = new SimpleAccount();
            account.setToken("");
            account.setPermissions(new ArrayList<>());
            account.setModel(false);
            accountManager.add(account);
        }
    }


    /**
     * 刷新
     */
    @Override
    public void refresh() {
        try (InputStream is = getIniStream()){
            String newFileMd5 = FileUtils.getFileMD5String(is);
            if (!newFileMd5.equals(iniFileMD5)){
                accountManager.refresh();
                onRun(is);
            }
        }catch (Exception e){
            System.out.println("刷新失败:"+e.getMessage());
        }
    }
}
