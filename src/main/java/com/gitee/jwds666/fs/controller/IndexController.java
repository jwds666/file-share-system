package com.gitee.jwds666.fs.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class IndexController {

    @GetMapping
    public String index(HttpServletRequest request){
        if (request.getHeader("User-Agent").indexOf("Mobile") != -1){
            return "mb/index";
        }
        return "index";
    }
}
