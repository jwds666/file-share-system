package com.gitee.jwds666.fs.version;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class RemoteFileShareJarVersion implements ShareJarVersion{

    /**
     * 获取版本号
     *
     * @param uri
     * @return
     */
    @Override
    public String getVersion(String uri) {
        HttpURLConnection connection;
        try {
            URL url = new URL(uri);
            connection = (HttpURLConnection) url.openConnection();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
