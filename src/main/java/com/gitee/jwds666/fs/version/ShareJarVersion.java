package com.gitee.jwds666.fs.version;

public interface ShareJarVersion {


    /**
     * 获取版本号
     * @param uri
     * @return
     */
    String getVersion(String uri);



}
