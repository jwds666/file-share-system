package com.gitee.jwds666.fs.account;

public abstract class AbstractAccountAuth implements AccountAuth{

    protected AccountManager manager;

    public AccountManager getManager() {
        return manager;
    }

    public void setManager(AccountManager manager) {
        this.manager = manager;
    }
}
