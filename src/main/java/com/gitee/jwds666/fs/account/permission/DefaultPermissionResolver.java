package com.gitee.jwds666.fs.account.permission;
/**
 * 文件夹以/相隔
 * 多项以|相隔
 * 操作以>相隔
 * 操作： s：查看，i：增加，u：修改，d：删除
 */
public class DefaultPermissionResolver implements PermissionResolver{


    @Override
    public Permission resolve(String permission) {
        Permission res = new Permission();
        String[] strAtr = permission.split(">");
        if (strAtr.length == 1 || strAtr[1].equals("")){
            res.setOperationPermission(new String[]{"*"});
        }else {
            res.setOperationPermission(strAtr[1].split("\\|"));
        }
        res.setDirPermission(strAtr[0]);
        return res;
    }
}
