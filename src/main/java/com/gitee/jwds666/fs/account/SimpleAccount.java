package com.gitee.jwds666.fs.account;

import com.gitee.jwds666.fs.account.permission.Permission;
import com.gitee.jwds666.fs.account.permission.PermissionResolver;

import java.util.List;

public class SimpleAccount implements Account{
    /**
     * 访问标识
     */
    private String token;

    /**
     * 匹配模式
     * true：白名单
     * false：黑名单
     */
    private boolean model;

    /**
     * 权限
     */
    private List<Permission> permissions;


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isModel() {
        return model;
    }

    public void setModel(boolean model) {
        this.model = model;
    }

    public List<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }


    /**
     * 获取账户标识
     *
     * @return
     */
    @Override
    public String getAccount() {
        return getToken();
    }

    /**
     * 是否包含权限
     *
     * @param permission
     * @return
     */
    @Override
    public boolean includes(String permission, PermissionResolver resolver) {
        return includes(resolver.resolve(permission));
    }

    /**
     * 是否包含权限
     *
     * @param permission
     * @return
     */
    @Override
    public boolean includes(Permission permission) {
        boolean flag = false;
        for (Permission compare : permissions) {
            if (compare.include(permission)){
                flag = true;
                break;
            }
        }
        return flag == model;
    }

    /**
     * 是否包含此权限的查看操作
     *
     * @param permission
     * @param resolver
     * @return
     */
    @Override
    public boolean includeSelect(String permission, PermissionResolver resolver) {
        return includes(permission+">s",resolver);
    }

    /**
     * 是否包含此权限的添加操作
     *
     * @param permission
     * @param resolver
     * @return
     */
    @Override
    public boolean includeInsert(String permission, PermissionResolver resolver) {
        return includes(permission+">i",resolver);
    }

    /**
     * 是否包含此权限的修改操作
     *
     * @param permission
     * @param resolver
     * @return
     */
    @Override
    public boolean includeUpdate(String permission, PermissionResolver resolver) {
        return includes(permission+">u",resolver);
    }

    /**
     * 是否包含此权限的删除操作
     *
     * @param permission
     * @param resolver
     * @return
     */
    @Override
    public boolean includeDelete(String permission, PermissionResolver resolver) {
        return includes(permission+">d",resolver);
    }

    public SimpleAccount() {
        model = true;
    }
}
