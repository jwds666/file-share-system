package com.gitee.jwds666.fs.account;

import javax.servlet.http.HttpServletRequest;

public class DefaultHttpAccountAuth extends HttpAccountAuth{
    /**
     * 认证
     *
     * @return
     */
    @Override
    public Account auth() {
        HttpServletRequest request = getRequest();
        String token = request.getHeader("share-file-token");
        if (token == null){
            token = request.getParameter("account");
        }
        Account res = getManager().get(token);
        if (res == null)res = getManager().get("");
        return res;
    }
}
