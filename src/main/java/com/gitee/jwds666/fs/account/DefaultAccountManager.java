package com.gitee.jwds666.fs.account;

import java.util.HashMap;
import java.util.Map;

public class DefaultAccountManager implements AccountManager{


    private Map<String,Account> cache = new HashMap<>();

    @Override
    public Account get(String token) {
        return cache.get(token);
    }

    @Override
    public Account add(Account account) {
        return cache.put(account.getAccount(),account);
    }


    /**
     * 刷新
     */
    @Override
    public void refresh() {
        cache.clear();
    }
}
