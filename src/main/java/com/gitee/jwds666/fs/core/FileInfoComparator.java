package com.gitee.jwds666.fs.core;


import java.util.Comparator;

public class FileInfoComparator implements Comparator<FileInfo> {
    @Override
    public int compare(FileInfo o1, FileInfo o2) {
        if (o1.getIsFile() != o2.getIsFile()){
            if (o1.getIsFile())return 1;
            if (o2.getIsFile())return -1;
        }
        String name1 = o1.getName();
        String name2 = o2.getName();
        return name1.compareToIgnoreCase(name2);
    }
}
