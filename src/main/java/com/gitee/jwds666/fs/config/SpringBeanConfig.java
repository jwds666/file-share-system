package com.gitee.jwds666.fs.config;

import com.gitee.jwds666.fs.account.AccountAuth;
import com.gitee.jwds666.fs.account.AccountManager;
import com.gitee.jwds666.fs.account.DefaultAccountManager;
import com.gitee.jwds666.fs.account.DefaultHttpAccountAuth;
import com.gitee.jwds666.fs.account.permission.PathPermissionResolver;
import com.gitee.jwds666.fs.account.permission.PermissionResolver;
import com.gitee.jwds666.fs.core.DefaultFileConvertFileInfo;
import com.gitee.jwds666.fs.core.FileConvertFileInfo;
import com.gitee.jwds666.fs.core.FileInfoComparator;
import com.gitee.jwds666.fs.env.Environment;
import com.gitee.jwds666.fs.env.ConfEnvironment;
import com.gitee.jwds666.fs.utils.InetUtils;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import java.util.Timer;
import java.util.TimerTask;

@Configuration
public class SpringBeanConfig implements ApplicationContextAware {

    @Resource(name = "fileShareEnvironment")
    private Environment environment;

    @Bean
    public AccountManager accountManager(){
        return new DefaultAccountManager();
    }

    @Bean
    public PermissionResolver permissionResolver(){
        return new PathPermissionResolver();
    }

    @Bean
    public FileConvertFileInfo fileConvertFileInfo(AccountAuth accountAuth, PermissionResolver permissionResolver, FileShareConfig config){
        return new DefaultFileConvertFileInfo(accountAuth,permissionResolver,config,new FileInfoComparator());
    }


    @Bean
    public AccountAuth accountAuth(AccountManager accountManager){
        DefaultHttpAccountAuth defaultHttpAccountAuth = new DefaultHttpAccountAuth();
        defaultHttpAccountAuth.setManager(accountManager);
        return defaultHttpAccountAuth;
    }

    @Bean("fileShareEnvironment")
    public Environment environment(AccountManager accountManager,PermissionResolver permissionResolver,FileShareConfig config){
        return new ConfEnvironment(accountManager,permissionResolver,config);
    }


    @Bean
    public void environmentRun(){
        environment.run();
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                try{
                    environment.refresh();
                }catch (Exception e){
                    System.err.println("环境刷新失败："+e.getMessage());
                }
            }
        },1000,1000);
    }



    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        String port = applicationContext.getEnvironment().getProperty("server.port");
        for (String s : InetUtils.getLocalHostIp()) {
            System.out.println("访问地址： http://"+s+":"+port);
        }
    }
}
