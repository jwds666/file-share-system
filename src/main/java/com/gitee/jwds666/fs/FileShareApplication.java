package com.gitee.jwds666.fs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class FileShareApplication  {


    public static void main(String[] args)  {
        System.out.println("启动中...");
        SpringApplication.run(FileShareApplication.class,args);
    }

}
